<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/* -----------------------------------------------*/

/* Rutas Tipo resource*/
Route::resource('register','usuariosController');
Route::resource('login', 'loginController');
Route::get('logout',[
	'uses' => 'loginController@logout',
	'as'   => 'logout'
]);


Route::group(['middleware' => 'auth'], function () {
	Route::get('/',[
		'uses'	=> 'homeController@index',
		'as'	=> 'index'
		]);
	Route::resource('noticias','noticiasController');
	Route::get('noticias/{id}/destroy',[
      'uses' => 'noticiasController@destroy',
      'as'   => 'noticias.destroy'
    ]);
    Route::get('noticias/{id}/ver',[
      'uses' => 'noticiasController@ver',
      'as'   => 'noticias.ver'
    ]);
    
	Route::resource('categorias','categoriasController');
    
    Route::resource('preguntas','preguntasController');
    Route::get('preguntas/{id}/destroy',[
      'uses' => 'preguntasController@destroy',
      'as'   => 'preguntas.destroy'
    ]);
     Route::get('preguntas/{id}/ver',[
      'uses' => 'preguntasController@ver',
      'as'   => 'preguntas.ver'
    ]);
    
    
    Route::resource('guias','guiasController');
    Route::get('guias/{id}/destroy',[
      'uses' => 'guiasController@destroy',
      'as'   => 'guias.destroy'
    ]);
      Route::get('guias/{id}/ver',[
      'uses' => 'guiasController@ver',
      'as'   => 'guias.ver'
    ]);
    
    
    Route::resource('ejercicios','ejerciciosController');
     Route::get('ejercicios/{id}/destroy',[
      'uses' => 'ejerciciosController@destroy',
      'as'   => 'ejercicios.destroy'
    ]);
     Route::get('ejercicios/{id}/ver',[
      'uses' => 'ejerciciosController@ver',
      'as'   => 'ejercicios.ver'
    ]);
    Route::resource('comentarios','comentariosController');

    Route::group(['prefix'=>'perfil', 'middleware' => 'auth'], function () {
      Route::get('/',[
        'uses' => 'perfilController@index',
        'as'   => 'perfil.index'
      ]);
      
      Route::resource('foto','fotoPerfilController');
    });
});



/* -----------------------------------------------*/