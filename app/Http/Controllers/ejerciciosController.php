<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Ejercicio;
class ejerciciosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $data = \App\Ejercicio::orderBy('created_at', 'desc')->paginate(20);
        return view('ejercicios.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('ejercicios.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           \App\Ejercicio::create([
			'titulo'=>$request['titulo'],
			'contenido'=>$request['contenido'],
            'autor' => Auth::user()->username,
            'dificultad' => $request['dificultad']
		]);
		return redirect()->route('index');
    }

    
       public function ver($id)
    {
        $ejer = \App\Ejercicio::find($id);
        return view('ejercicios.ver',compact('ejer'));
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = ejercicio::find($id);
        return view('ejercicios.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $ejercicio = \App\Ejercicio::find($id);
        $ejercicio-> fill($request->all())->save();
        return redirect()->route('index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Ejercicio::destroy($id);
        return back();
    }
}
