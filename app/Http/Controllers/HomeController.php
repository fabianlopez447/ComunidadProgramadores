<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $guias = \App\Guia::orderBy('created_at', 'DESC')->paginate(2);
        $data = \App\Noticia::orderBy('created_at', 'desc')->paginate(5);
        return view('index', compact('data', 'guias'));
    }
}
