<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use \App\Noticia;
class noticiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$data = \App\Noticia::orderBy('created_at', 'desc')->paginate(20);
        return view('index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         $categoria = \App\Categoria::orderBy('nombre', 'desc')->get();
         return view('noticias.create',compact('categoria'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = $request->file('imagen');
        $foto = Auth::user()->username.'.'.$file->getClientOriginalExtension();
        $file->move(public_path().'/img/noticias', $foto);

        \App\Noticia::create([
			'titulo'=>$request['titulo'],
			'contenido'=>$request['contenido'],
            'autor' => Auth::user()->username,
            'categoria'=>$request['categoria'],
            'imagen' => $foto
		]);
		return redirect()->route('index');
    }

    public function ver($id)
    {
        $not = \App\Noticia::find($id);
        return view('noticias.ver',compact('not'));
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Noticia::find($id);
        $categoria = \App\Categoria::orderBy('nombre', 'desc')->get();
        return view('noticias.edit', compact('data', 'categoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $noticia = \App\Noticia::find($id);
        $noticia-> fill([
            'titulo'=>$request['titulo'],
            'contenido'=>$request['contenido'],
            'autor' => Auth::user()->username,
            'categoria'=>$request['categoria'],
        ])->save();
        return redirect()->route('index');
    }
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Noticia::destroy($id);
        return back();
    }
}
