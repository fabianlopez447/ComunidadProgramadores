<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Guia;
class guiasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $data = \App\Guia::orderBy('created_at', 'desc')->paginate(20);
        return view('index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('guias.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           \App\Guia::create([
			'titulo'=>$request['titulo'],
			'contenido'=>$request['contenido'],
            'autor' => Auth::user()->username,
		]);
		return redirect()->route('index');
    }
    
       public function ver($id)
    {
        $gui = \App\Guia::find($id);
        return view('guias.ver',compact('gui'));
    }
    

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = guia::find($id);
        return view('guias.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $guia = \App\Guia::find($id);
        $guia-> fill($request->all())->save();
        return redirect()->route('index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \App\Guia::destroy($id);
        return back();
    }
}