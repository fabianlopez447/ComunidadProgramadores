<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ejercicio extends Model
{
    protected $table = 'ejercicios';
	protected $fillable = ['titulo','contenido','dificultad','autor'];
}
