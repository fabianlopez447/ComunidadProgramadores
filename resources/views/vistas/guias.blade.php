<?php $data = \App\Guia::orderBy('created_at', 'desc')->paginate(20); ?>
@foreach($data as $dato)
	@if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '2') || (Auth::user()->tipo == '3') || (Auth::user()->username == $dato->autor))
	@endif
	
	<div style="border-radius:3px;"  class="ui segment ">
		<h3 class="ui header paginacion" style="padding-bottom: 1em;">
            <a style="color: #2c3e50; text-decoration: none;" href="{{route('guias.ver',$dato->id)}}">{{ $dato->titulo }}</a>

            @if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '2') || (Auth::user()->tipo == '3') || (Auth::user()->username == $dato->autor))
				<div style="float: right;"class="ui dropdown">
				  	<i  style="" class="setting icon"></i>
				  	<div class="menu">
					    <a class="item" href="{{route('guias.edit', $dato->id)}}"> Editar</a>
					    <a class="item" href="{{route('guias.destroy', $dato->id)}}"> Borrar</a>
					</div>
				</div>
			@endif
		</h3>
		<i class="ui user icon"></i>{{ $dato->autor }}| <i class="ui calendar outline icon"></i>{!!date('d/m', strtotime($dato->created_at))!!} del {!!date('Y', strtotime($dato->created_at))!!} | <i class="ui wait icon"></i>{!!date('H:i', strtotime($dato->created_at))!!} | <i class="ui icon list layout"></i> {{ $dato->categoria }}
	</div>
@endforeach