@extends('layouts.template')
@section('content')
<div class="container ui">
		<div class="ui grid">
			<div class="three column row">	
				<!-- Perfil de Usuario -->
				<div class="column three wide">
					@include('secciones.perfil')
				<!-- Ultimas Preguntas-->
				    @include('secciones.preguntas')
				</div>
				<div class="thirteen wide column ">
					<div style="border-radius:0px;"  class="ui raised segment ">
						{!!Form::open(['route'=>'preguntas.store','method'=>'POST', 'class'=>'ui form ingresar', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
						<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
							<div class="ui">
							<h2 class="ui header paginacion">¿Qué quieres preguntar?</h2>
							<h2 class="ui header">
								<label style="font-size: 14px;">Titulo de la pregunta</label>
								{!!Form::text('titulo', null,['placeholder'=>'', 'style'=>'background: none; font-size:14px; padding-left: none;margin-left: none;'])!!}
							</h2>
							<!--
							<sub><i class="ui user icon"></i>{{ Auth::user()->username }}</sub> | <i class="ui calendar outline icon"></i>{{ date('d/m/Y') }} | <i class="ui wait icon"></i>{{ date('H:i')}}<br><br>-->

							{!!Form::textarea('contenido', null,['placeholder'=>'¿Cual es tu pregunta?... Ejemp:Lorem ipsum dolor sit amet...', 'style'=>'border: none; border-radius: none; background: none; font-size:14px; padding-left: none;margin-left: none;','id'=>'editor'])!!}
						</div>
						<br>
							<button type="submit" class="ui primary submit button">Aceptar</button>
					   {!!Form::close()!!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')

@endsection