<?php
use App\Comentario;
$coment = \App\Comentario::where('idpublicacion',$gui->id)->paginate(2000);
?>
@extends('layouts.template')
@section('content')
<div class="container ui">
		<div class="ui grid">
			<div class="three column row">	
				<!-- Perfil de Usuario -->
				<div class="column three wide">
					@include('secciones.perfil')
				<!-- Ultimas guiuntas-->
				    @include('secciones.preguntas')
				</div>
				<div class="thirteen wide column ">
					<div style="border-radius:0px;"  class="ui raised segment ">					
						<h2 class="ui header paginacion">
							{!!$gui->titulo!!}<hr>
						</h2>
						<?php
							$contenido = str_replace("&lt;", "<", $gui->contenido);
							$contenido = str_replace("&gt;", ">", $contenido);
						?>
						<p>{!!$contenido!!}</p>	
						    <hr>
							<div class="ui">	
							     <i class="ui user icon"></i>{!!$gui->autor!!}| <i class="ui calendar outline icon"></i>{!!date('d/m', strtotime($gui->created_at))!!} del {!!date('Y', strtotime($gui->created_at))!!}| <i class="ui wait icon"></i>{!!date('H:i', strtotime($gui->created_at))!!}<br><br>	
						    </div>
						    
					</div>
				<div class="ui minimal comments">
                      <h3 class="ui dividing header">Comentarios</h3>
                      @foreach($coment as $coments)
                       <div class="comment">
                        <a class="avatar">
                          <img src="/images/avatar/small/joe.jpg">
                        </a>
                        <div class="content">
                          <a class="author">{!!$coments->autor!!}</a>
                          <div class="metadata">
                            <span class="date">{!!date('d/m', strtotime($coments->created_at))!!} del {!!date('Y', strtotime($coments->created_at))!!}</span>
                          </div>
                          <div class="text">
                            {!!$coments->comentario!!}
                          </div>
                        </div>
                      </div>
                      @endforeach
                      {!!Form::open(['route'=>'comentarios.store','method'=>'POST', 'class'=>'ui form ingresar', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
                        <div class="field">
                          {!!Form::textarea('comentario', null,['placeholder'=>'Aquí tu comentario'])!!}
                          {!!Form::hidden('seccion', 'guias')!!}
                          {!!Form::hidden('idpublicacion', $gui->id)!!}
                        </div>
                        <button class="ui blue labeled submit icon button"> <i class="icon edit"></i> Agregar Comentario</button>
                      {!!Form::close()!!}
                    </div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')

@endsection