@extends('layouts.template')
@section('content')
<div class="container ui">
		<div class="ui grid">
			<div class="three column row">	
				<!-- Perfil de Usuario -->
				<div class="column three wide">
					@include('secciones.perfil')
				</div>
				<div class="thirteen wide column ">
					<div style="border-radius:0px;"  class="ui ">
							<div class="ui horizontal divider">
								<h2 class="ui center aligned icon">
									<i class="mail icon"></i>Editar guia
								</h2>
							</div>	
						{!!Form::model($data, ['route'=>['guias.update', $data->id],'method'=>'PUT', 'class'=>'ui form ingresar'])!!}
						<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
						<div class="ui">
							<h2 class="ui header">
								{!!Form::text('titulo', null,['placeholder'=>'Titulo de la guia', 'style'=>'border: none; background: none; font-size:20px; padding-left: none;margin-left: none;'])!!}
							</h2>
							<sub><i class="ui user icon"></i>{{ Auth::user()->username }}</sub> | <i class="ui calendar outline icon"></i>{{ date('d/m/Y') }} | <i class="ui wait icon"></i>{{ date('H:i')}}<br><br>
							{!!Form::textarea('contenido', null,['placeholder'=>'Contenido de tu guia... Ejemp:Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut facilis harum voluptatem, fugiat, assumenda illum reprehenderit dolorem non molestias praesentium, provident, nesciunt earum autem doloremque in consequatur ullam. Eos, eveniet! ', 'style'=>'border: none; border-radius: none; background: none; font-size:14px; padding-left: none;margin-left: none;','id'=>'editor'])!!}
						</div>
							<button type="submit" class="ui primary submit button">Aceptar</button>	
					   {!!Form::close()!!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')
@endsection