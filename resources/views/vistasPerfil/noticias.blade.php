<div style="border-radius:3px;"  class="ui raised segment border-card p-bottom-lg shadow-bottom">				
	
@foreach($data as $dato)
<?php
	$autor = \App\User::where('username', $dato->autor)->first();
 ?>
<div class="ui segment piled " style="border-top:none;border-bottom: 1px solid #bdc3c7;border-left: none;border-right: none; margin-top: 0px; box-shadow: none;">
	<h3 class="ui header paginacion">
	 @if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '2') || (Auth::user()->tipo == '3') || (Auth::user()->username == $dato->autor))
			<div style="float: right;" class="ui dropdown">
			  	<i  style="color: #6BC0E9;" class="setting icon"></i>
			  	<div class="menu">
				    <a class="item" href="{{route('noticias.edit', $dato->id)}}"> Editar</a>
				    <a class="item" href="{{route('noticias.destroy', $dato->id)}}"> Eliminar</a>
				</div>
			</div>
		@endif
		<h2 class="ui header">
		  <img src="{{asset('/img/perfil/'.$autor->foto)}}" class="ui circular image">
		  	<a style="color: #2c3e50; text-decoration: none;" href="{{route('noticias.ver',$dato->id)}}">
		  		{{ $dato->titulo }} <br>
		  	</a>
		  <small>
		  	<sub>
		  		<i class="ui user icon"></i>{{ $dato->autor }}| <i class="ui calendar outline icon"></i>{!!date('d/m', strtotime($dato->created_at))!!} del {!!date('Y', strtotime($dato->created_at))!!} | <i class="ui wait icon"></i>{!!date('H:i', strtotime($dato->created_at))!!} | <i class="ui icon list layout"></i> {{ $dato->categoria }}
		  	</sub>
		  </small>
		</h2>
        <a style="color: #2c3e50; text-decoration: none;" href="{{route('noticias.ver',$dato->id)}}">
			<img class="ui fluid image" src="{{asset('img/noticias/'.$dato->imagen)}}" width="100%" alt="">
		</a>
       
	</h3>
	<?php
		$contenido = str_replace("&lt;", "<", $dato->contenido);
		$contenido = str_replace("&gt;", ">", $dato->contenido);
	?>
	{!!substr($contenido,0,255)!!}...
	
</div>
@endforeach
</div>
