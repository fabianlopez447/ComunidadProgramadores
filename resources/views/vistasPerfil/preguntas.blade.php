<?php $data = \App\Pregunta::orderBy('created_at', 'desc')->where('autor', Auth::user()->username)->paginate(20); ?>
	@foreach($data as $dato)
	@if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '2') || (Auth::user()->tipo == '3') || (Auth::user()->username == $dato->autor))
	
	@endif
	
	<div style="border-radius:3px;"  class="ui  segment border-card p-top-lg p-bottom-lg shadow-bottom">
		<h3 class="ui header paginacion">
			<?php $foto = \App\User::where('username', $dato->autor)->first(); ?>
			<img class="ui top aligned tiny circular image img-section-for" style="width: 32px; height: 32px;" src="{!!asset('img/perfil/'.$foto->foto)!!}">
            <a style="color: #2c3e50; text-decoration: none;" href="{{route('preguntas.ver',$dato->id)}}">{{ $dato->titulo }}</a>

            @if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '2') || (Auth::user()->tipo == '3') || (Auth::user()->username == $dato->autor))
				<div style ="float: right;" class="ui dropdown">
				  	<i  style="color: #6BC0E9;" class="setting icon"></i>
				  	<div class="menu">
					    <a class="item" href="{{route('preguntas.edit', $dato->id)}}"> Editar</a>
					    <a class="item" href="{{route('preguntas.destroy', $dato->id)}}"> Borrar</a>
					</div>
				</div>
			@endif
		</h3>
		<i class="ui user icon"></i>{{ $dato->autor }} | <i class="ui calendar outline icon"></i>{!!date('d/m', strtotime($dato->created_at))!!} del {!!date('Y', strtotime($dato->created_at))!!} | <i class="ui wait icon"></i>{!!date('H:i', strtotime($dato->created_at))!!}
	</div>
@endforeach