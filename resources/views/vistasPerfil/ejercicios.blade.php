<?php $data = \App\Ejercicio::orderBy('created_at', 'desc')->where('autor', Auth::user()->username)->paginate(20); ?>
@foreach($data as $dato)
	@if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '2') || (Auth::user()->tipo == '3') || (Auth::user()->username == $dato->autor))
	
	@endif
	
	<div style="border-radius:3px;"  class="ui segment ">
		<h3 class="ui header paginacion">
            <a style="color: #2c3e50; text-decoration: none;" href="{{route('ejercicios.ver',$dato->id)}}">{{ $dato->titulo }}</a>
            @if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '2') || (Auth::user()->tipo == '3') || (Auth::user()->username == $dato->autor))
				<div style="float: right;" class="ui dropdown">
				  	<i  style="color: #6BC0E9;" class="setting icon"></i>
				  	<div class="menu">
					    <a class="item" href="{{route('ejercicios.edit', $dato->id)}}"> Editar</a>
					    <a class="item" href="{{route('ejercicios.destroy', $dato->id)}}"> Borrar</a>
					</div>
				</div>
			@endif
		</h3>
		<i class="ui user icon"></i>{{ $dato->autor }}| <i class="ui calendar outline icon"></i>{!!date('d/m', strtotime($dato->created_at))!!} del {!!date('Y', strtotime($dato->created_at))!!} | <i class="ui wait icon"></i>{!!date('H:i', strtotime($dato->created_at))!!} | <i class="ui icon signal"></i> {{ $dato->dificultad }}
	</div>
@endforeach