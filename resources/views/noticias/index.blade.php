@extends('layouts.template')
@section('content')
	<div class="container ui">
		<div class="ui grid">
			<div class="three column row">	
				<!-- Perfil de Usuario -->
				<div class="column three wide">
					@include('secciones.perfil')
				<!-- Ultimas Preguntas-->
				    @include('secciones.preguntas')
				</div>
				<div class="nine wide column ">
					<h2 class="ui header paginacion">Noticias</h2>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua.</p>
						
					@foreach($data as $dato)
					@if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '2') || (Auth::user()->tipo == '3') || (Auth::user()->username == $dato->autor))
						<a href="{{route('noticias.create')}}" class="fluid ui button orange">Publicar una noticia</a>
					@endif
					
					<div style="border-radius:3px;"  class="ui segment ">
						<h3 class="ui header paginacion">
				            <a style="color: #2c3e50; text-decoration: none;" href="{{route('noticias.ver',$dato->id)}}">{{ $dato->titulo }}</a>
				            @if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '2') || (Auth::user()->tipo == '3') || (Auth::user()->username == $dato->autor))
								<div style="float: right;" class="ui dropdown">
								  	<i  style="color: #76C4EA;" class="setting icon"></i>
								  	<div class="menu">
									    <a class="item" href="{{route('noticias.edit', $dato->id)}}"> Editar</a>
									    <a class="item" href="{{route('noticias.destroy', $dato->id)}}"> Borrar</a>
									</div>
								</div>
							@endif
						</h3>
						
						{!!substr($dato->contenido,0,255)!!}...
						<br><br>
						<i class="ui user icon"></i>{{ $dato->autor }}| <i class="ui calendar outline icon"></i>{!!date('d/m', strtotime($dato->created_at))!!} del {!!date('Y', strtotime($dato->created_at))!!} | <i class="ui wait icon"></i>{!!date('H:i', strtotime($dato->created_at))!!} | <i class="ui icon list layout"></i> {{ $dato->categoria }}
					</div>
					@endforeach
				</div>
				<div  class="four wide column">
					<!-- Ultimos ejercicios -->
					@include('secciones.ejercicios')	
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')

@endsection