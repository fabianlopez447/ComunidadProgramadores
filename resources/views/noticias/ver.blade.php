<?php
use App\Comentario;
$coment = \App\Comentario::where('idpublicacion',$not->id)->where('seccion','noticias')->get();
?>
@extends('layouts.template')
@section('header')
<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
@endsection
@section('content')
<div class="container ui">
		<div class="ui grid">
			<div class="three column row">	
				<!-- Perfil de Usuario -->
				<div class="column three wide">
					@include('secciones.perfil')
				<!-- Ultimas Preguntas-->
				    @include('secciones.preguntas')
				</div>
				<div class="thirteen wide column ">				
					<?php
						$autor = \App\User::where('username', $not->autor)->first();
					 ?>
					<div style="border-radius:0px; font-size: 16px; font-family: 'Lato', serif;color: black;" class="ui segment">
							@if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '2') || (Auth::user()->tipo == '3') || (Auth::user()->username == $not->autor))
								<div style="float: right;" class="ui dropdown">
								  	<i class="setting icon"></i>
								  	<div class="menu">
									    <a class="item" href="{{route('noticias.edit', $not->id)}}"> Editar</a>
									    <a class="item" href="{{route('noticias.destroy', $not->id)}}"> Borrar</a>
									</div>
								</div>
							@endif
							<h2 class="ui header paginacion">
								<h2 class="ui header">
								  <img src="{{asset('/img/perfil/'.$autor->foto)}}" class="ui circular image">
								  	<a style="color: #2c3e50; text-decoration: none;" href="{{route('noticias.ver',$not->id)}}">
								  		{{ $not->titulo }} <br>
								  	</a>
								  <small>
								  	<sub>
								  		<i class="ui user icon"></i>{{ $not->autor }}| <i class="ui calendar outline icon"></i>{!!date('d/m', strtotime($not->created_at))!!} del {!!date('Y', strtotime($not->created_at))!!} | <i class="ui wait icon"></i>{!!date('H:i', strtotime($not->created_at))!!} | <i class="ui icon list layout"></i> {{ $not->categoria }}
								  	</sub>
								  </small>
								</h2>
						        <a style="color: #2c3e50; text-decoration: none;" href="{{route('noticias.ver',$not->id)}}">
									<img class="ui fluid image" src="{{asset('img/noticias/'.$not->imagen)}}" width="100%" alt="">
								</a>
						</h2>
						<?php
							$contenido = str_replace("&lt;", "<", $not->contenido);
							$contenido = str_replace("&gt;", ">", $contenido);
						?>
						<p>{!!$contenido!!}</p>	
						
						    
					</div>
					<div class="ui very padded segment">
				     <div class="ui comments" style="margin-left: auto; margin-right: auto;">
                      <h3 class="ui dividing header">Comentarios</h3>
                      @foreach($coment as $coments)
                      	<?php $user = \App\User::where('username', $coments->autor)->get(); ?>
                       <div class="comment">
                        <a class="avatar">
                        	@foreach($user as $us)
                          		<img src="{!!asset('img/perfil/'.$us->foto)!!}">
                          	@endforeach
                        </a>
                        <div class="content">
                          <a class="author">{!!$coments->autor!!}</a>
                          <div class="metadata">
                            <span class="date">{!!date('d/m', strtotime($coments->created_at))!!} del {!!date('Y', strtotime($coments->created_at))!!}</span>
                          </div>
                          <div class="text">
                            {!!$coments->comentario!!}
                          </div>
                        </div>
                      </div>
                      @endforeach
                      {!!Form::open(['route'=>'comentarios.store','method'=>'POST', 'class'=>'ui form ingresar', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
                        <div class="field">
                          {!!Form::textarea('comentario', null,['placeholder'=>'Aquí tu comentario'])!!}
                          {!!Form::hidden('seccion', 'noticias')!!}
                          {!!Form::hidden('idpublicacion', $not->id)!!}
                        </div>
                        <button class="ui blue labeled submit icon button"> <i class="icon edit"></i> Comentar</button>
                      {!!Form::close()!!}
                    </div>
                    </div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')

@endsection