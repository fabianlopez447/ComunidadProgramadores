@extends('layouts.template')
@section('content')
<div class="container ui">
		<div class="ui grid">
			<div class="three column row">	
				<!-- Perfil de Usuario -->
				<div class="column three wide">
					@include('secciones.perfil')
				<!-- Ultimas Preguntas-->
				    @include('secciones.preguntas')
				</div>
				
				<div class="nine wide column ">
				<h2 class="ui header paginacion">
					Nueva noticia
				</h2>	
				{!!Form::open(['route'=>'noticias.store','method'=>'POST', 'class'=>'ui form ingresar', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
					<div  class="ui raised segment ">	
						<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
						<div class="ui">
							<h2 class="ui header">
								{!!Form::text('titulo', null,['placeholder'=>'Titulo de la nueva noticia', 'style'=>'font-size:16px;color:#484848;'])!!}
							</h2>
						</div>
					</div>
					<i class="ui user icon"></i>{{ Auth::user()->username }}| <i class="ui calendar outline icon"></i>{{ date('d/m/Y') }} | <i class="ui wait icon"></i>{{ date('H:i')}}
					<div  class="ui raised segment ">
						<div class="ui">
							{!!Form::textarea('contenido', null,['placeholder'=>'Contenido de tu noticia... Ejemp:Lorem ipsum dolor sit amet...', 'style'=>'border: none; border-radius: none; background: none; font-size:14px; padding-left: none;margin-left: none;','id'=>'editor'])!!}
						</div>
					</div>
				</div>
				<div  class="four wide column">
					<!-- Publicar -->
					<div class="ui card">
						<div class="content">
							<div class="header"> <center> Publicar</center></div>
						</div>
						<div class="extra content">
							<button type="submit" class="ui primary submit button">Publicar</button>
						</div>
					</div>	
					<!-- Categoría -->
					<div class="ui card">
						<div class="content">
							<div class="header"> <center> Categoría</center></div>
						</div>
						<div class="content">
							@foreach($categoria as $categorias)
								<div class="ui radio checkbox">
									<input type="radio" checked name="categoria" id="{!!$categorias->id!!}" value="{!!$categorias->id!!}"> <label for="{!!$categorias->id!!}">{!!$categorias->nombre!!}</label>
								</div>
								<br><br>
							@endforeach
						</div>
						<div class="extra content">
							<div class="form-group">
								<a href="{{route('categorias.create')}}" class="fluid ui teal button">Añadir nuevas</a>
							</div>
						</div>
					</div>	
					<!-- Imagen -->
					<div class="ui card">
						<div class="content">
							<div class="header"> <center> Imagen destacada</center></div>
						</div>
						<div class="content">
							<input id="foto" type="file" name="imagen" onchange="readURL(this);" style="display: none;">
							<label for="foto">
								<img src="" id="prev" alt="" width="100%">
								<center>Añadir imagen destacada</center>
							</label>
						</div>
					</div>	
				</div>
			{!!Form::close()!!}
			</div>
		</div>
	</div>
@endsection
@section('js')
<script>
 $('select.dropdown')
  .dropdown()
;
function readURL(input) {
 if (input.files && input.files[0]) {
     var reader = new FileReader();
     reader.onload = function (e) {
        $('#prev').attr('src', e.target.result);
     }
 
    reader.readAsDataURL(input.files[0]);
 }
}

</script>
@endsection