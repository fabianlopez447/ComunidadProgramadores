<!DOCTYPE html>
<html>
@include('elementos.header')
<body>
<!-- Navbar -->
@include('elementos.navbar')
<!-- Fin Navbar -->	
<div class="container ui">
	@yield('content')
</div>

<script src="{!!asset('js/jquery.min.js')!!}"></script>
<script src="{!!asset('js/semantic.min.js')!!}"></script>
<script src="{!!asset('js/main.js')!!}"></script>
<script src="{!!asset('ckeditor/ckeditor.js')!!}"></script>
<script type="text/javascript">
$('.menu.pestana .item')
  .tab()
;
CKEDITOR.replace ("editor");
</script>
@yield('js')
</body>
</html>