<!DOCTYPE html>
<html>
@include('elementos.header')
<body>
<!-- Navbar -->
@include('elementos.navbar')
<!-- Fin Navbar --> 
<div class="container ui">
    @yield('content')
</div>
  
</body>
<script src="{!!asset('js/jquery.min.js')!!}"></script>
<script src="{!!asset('js/semantic.min.js')!!}"></script>
<script src="{!!asset('js/main.js')!!}"></script>
<script src="{!!asset('ckeditor/ckeditor.js')!!}"></script>
<script type="text/javascript">
CKEDITOR.replace ("editor");
</script>
@yield('js')
</html>