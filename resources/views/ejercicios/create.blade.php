@extends('layouts.template')
@section('content')
<div class="container ui">
		<div class="ui grid">
			<div class="three column row">	
				<!-- Perfil de Usuario -->
				<div class="column three wide">
					@include('secciones.perfil')
				<!-- Ultimas ejercicios-->
				    @include('secciones.ejercicios')
				</div>
				<div class="thirteen wide column ">
					<div style="border-radius:0px;"  class="ui raised segment ">	
						<h2 class="ui header paginacion">
							Nuevo ejercicio<hr>
						</h2>	
						{!!Form::open(['route'=>'ejercicios.store','method'=>'POST', 'class'=>'ui form ingresar', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
						<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
							<div class="ui">
							
							<h2 class="ui header">
								<label style="font-weight: bold;font-size: 14px;">Titulo de ejercicio</label>
								{!!Form::text('titulo', null,['placeholder'=>'', 'style'=>'background: none; font-size:16px; padding-left: none;margin-left: none;'])!!}
							</h2>
							<div class="field">
							<label for="dificultad">Dificultad</label>
							<select name="dificultad" id="dificultad">
								<option value="Facil">Fácil</option>
								<option value="Medio">Medio</option>
								<option value="Dificil">Díficil</option>
							</select>
						</div>
							{!!Form::textarea('contenido', null,['placeholder'=>'¿Cual es tu ejercicio?... Ejemp:Lorem ipsum dolor sit amet...', 'style'=>'border: none; border-radius: none; background: none; font-size:14px; padding-left: none;margin-left: none;','id'=>'editor'])!!}
						</div>
						
						<br>
							<button type="submit" class="ui primary submit button">Aceptar</button>
					   {!!Form::close()!!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')

@endsection