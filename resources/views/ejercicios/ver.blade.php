<?php
use App\Comentario;
$coment = \App\Comentario::where('idpublicacion',$ejer->id)->where('seccion','ejercicios')->paginate(2000);
?>

@extends('layouts.template')
@section('content')
<div class="container ui">
		<div class="ui grid">
			<div class="three column row">	
				<!-- Perfil de Usuario -->
				<div class="column three wide">
					@include('secciones.perfil')
				<!-- Ultimas ejeruntas-->
				    @include('secciones.preguntas')
				</div>
				<div class="thirteen wide column ">
					<h2 class="ui center aligned icon">
							{!!$ejer->titulo!!}
								@if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '2') || (Auth::user()->tipo == '3') || (Auth::user()->username == $ejer->autor))
									<div style="float: right;" class="ui dropdown">
									  	<i class="setting icon"></i>
									  	<div class="menu">
										    <a class="item" href="{{route('ejercicios.edit', $ejer->id)}}"> Editar</a>
										    <a class="item" href="{{route('ejercicios.destroy', $ejer->id)}}"> Borrar</a>
										</div>
									</div>
								@endif
						</h2>	
						<div class="ui">	
						     <sub><i class="ui user icon"></i>{!!$ejer->autor!!}</sub>  | <i class="ui calendar outline icon"></i>{!!date('d/m', strtotime($ejer->created_at))!!} del {!!date('Y', strtotime($ejer->created_at))!!}| <i class="ui wait icon"></i>{!!date('H:i', strtotime($ejer->created_at))!!} | <i class="ui icon signal"></i> {{ $ejer->dificultad }}<br><br>	
					    </div>
					<div style="border-radius:0px; font-size: 16px; font-family: 'Lora', serif;color: black;" class="ui segment">
						    <?php
							$contenido = str_replace("&lt;", "<", $ejer->contenido);
							$contenido = str_replace("&gt;", ">", $contenido);
						?>
						<p>{!!$contenido!!}</p>	
					</div>
					<div class="ui minimal comments">
                      <h3 class="ui dividing header">Comentarios</h3>
                      @foreach($coment as $coments)
                       <?php $user = \App\User::where('username', $coments->autor)->get(); ?>
                       <div class="comment">
                        <a class="avatar">
                        	@foreach($user as $us)
                          		<img src="{!!asset('img/perfil/'.$us->foto)!!}">
                          	@endforeach
                        </a>
                        <div class="content">
                          <a class="author">{!!$coments->autor!!}</a>
                          <div class="metadata">
                            <span class="date">{!!date('d/m', strtotime($coments->created_at))!!} del {!!date('Y', strtotime($coments->created_at))!!}</span>
                          </div>
                          <div class="text">
                            {!!$coments->comentario!!}
                          </div>
                        </div>
                      </div>
                      @endforeach
                      {!!Form::open(['route'=>'comentarios.store','method'=>'POST', 'class'=>'ui form ingresar', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
                        <div class="field">
                          {!!Form::textarea('comentario', null,['placeholder'=>'Aquí tu comentario'])!!}
                          {!!Form::hidden('seccion', 'ejercicios')!!}
                          {!!Form::hidden('idpublicacion', $ejer->id)!!}
                        </div>
                        <button class="ui blue labeled submit icon button"> <i class="icon edit"></i> Agregar Comentario</button>
                      {!!Form::close()!!}
                    </div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')

@endsection