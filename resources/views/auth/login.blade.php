@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui grid">
        <div class="four wide column"></div>
        <div class="seven wide column">
            <div class="ui segment stacked p-lg shadow-bottom " id="fondo-login">
                    {!!Form::open(['method'=>'POST', 'route'=>'login.store', 'class'=>'ui large form '])!!}
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <div class="field {{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="">Email</label>
                                <input id="email" type="email" name="email" placeholder="Ej: jseanvzla@gmail.com" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <span class="ui message">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>
                        <div class="field {{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="">Contraseña</label>
                                <input id="password" type="password" class="form-control" name="password" placeholder="Contraseña" required>
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="field">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Recordar contraseña
                                    </label>
                                </div>
                        </div>

                        <button type="submit" class="ui fluid large teal submit button">
                            Aceptar
                        </button>
                        <br>
                        <a class="" href="{{ url('/password/reset') }}">
                            ¿Olvidaste tu contraseña?
                        </a>
                    {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection
