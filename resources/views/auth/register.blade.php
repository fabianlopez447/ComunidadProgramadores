@extends('layouts.app')

@section('content')
<div class="container">
    <div class="ui grid">
        <div class="four wide column"></div>
        <div class="seven wide column">
            <div class="ui segment stacked p-lg shadow-bottom">
                    {!!Form::open(['method'=>'POST', 'route'=>'register.store', 'class'=>'ui large form'])!!}
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <div class="field">
                                <label for="">Usuario</label>
                                <input id="name" type="text" name="username" placeholder="Ej: jrodriguez" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="ui message">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                        </div>
                             <div class="field">
                                <label for="">Nombre</label>
                                <input id="nombre" type="text" name="nombre" placeholder="Ej: Juan Silva"  required autofocus>

                                @if ($errors->has('nombre'))
                                    <span class="ui message">
                                        <strong>{{ $errors->first('nombre') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="field">

                            <div class="col-md-6">
                                <div class="field">
                                    <label for="">Email</label>
                                    <input id="email" type="email" name="email" placeholder="Ej: mail@gmail.com"  required>

                                    @if ($errors->has('email'))
                                        <span class="ui message">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="field">

                            <div class="col-md-6">
                                <div class="field">
                                    <label for="">Contraseña</label>
                                    <input id="password" type="password" name="password" placeholder="Contraseña" required>

                                    @if ($errors->has('password'))
                                        <span class="ui message">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="field">
                                <label for="">Confirmar contraseña</label>
                                <input id="password-confirm" type="password" name="password_confirmation" placeholder="Confirmar contraseña" required>
                        </div>

                                <button type="submit" class="ui fluid large teal submit button">
                                    Registrarse
                                </button>
                    {!!Form::close()!!}
            </div>
        </div>
    </div>
</div>
@endsection
