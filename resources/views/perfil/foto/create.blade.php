@extends('layouts.template')
@section('content')
{!!Form::model($perfil,['method'=>'PUT', 'route'=>['perfil.foto.update',$perfil->id], 'class'=>'form','files' => true, 'enctype'=>'multipart/form-data'])!!}
	<div class="ui field">
		<label for="foto">Selecciona la foto</label><br>
		{!!Form::file('foto', ['id','foto'])!!}
	</div>
	<br>
	<div class="ui field">
		<button type="submit" class="ui button green">Cambiar</button>
	</div>
{!!Form::close()!!}
@endsection