@extends('layouts.template')
@section('content')
<div class="container ui">
		<div class="ui grid">
			<div class="three column row">	
				<!-- Perfil de Usuario -->
				<div class="column three wide">
				<!-- Ultimas Preguntas-->
				    @include('secciones.preguntas')
				</div>
				<div class="nine wide column ">
                   <div class="ui horizontal divider">
								<h2 class="ui center aligned icon">
									<i class="user icon"></i>Editar Información de usuario
								</h2>
							</div>	
						{!!Form::model($data, ['route'=>['perfil.update', $data->id],'method'=>'PUT', 'class'=>'ui form ingresar'])!!}
						
						<h3 class="ui header">Nombre
								{!!Form::text('nombre', null,['placeholder'=>'Nombre', 'style'=>'font-size:20px; padding-left: none;margin-left: none;'])!!}
							</h3>
				        <h3 class="ui header">Username
								{!!Form::text('username', null,['placeholder'=>'Nombre', 'style'=>'font-size:20px; padding-left: none;margin-left: none;'])!!}
							</h3>
				        <h3 class="ui header">GitHub
								{!!Form::text('github', null,['placeholder'=>'Nombre', 'style'=>'font-size:20px; padding-left: none;margin-left: none;'])!!}
							</h3>
				      <button type="submit" class="ui primary submit button">Aceptar</button>	
						
				        {!!Form::close()!!}
                </div>
				<div  class="four wide column">
					<!-- Ultimos ejercicios -->
					@include('secciones.ejercicios')	
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')

@endsection