@extends('layouts.template')
@section('content')
	<div class="container ui">
		<div class="ui grid">
			<div class="three column row">	
				<!-- Perfil de Usuario -->
				<div class="column four wide">
					@include('secciones.perfil')
					<!-- Ultimas Preguntas-->
				    @include('secciones.preguntas')
				</div>
				<div class="twelve wide column ">
					@include('secciones.usuario')
				</div>
			</div>
		</div>
	</div>
@endsection