<div class="ui labeled icon attached tabular menu pestana">
  <a class="item active" data-tab="first">
    <i class="newspaper icon"></i>
    Noticias
  </a>
  <a class="item" data-tab="second">
    <i class="help icon"></i>
    Preguntas
  </a>
  <a class="item" data-tab="third">
    <i class="rocket icon"></i>
    Retos
  </a>
  <a class="item" data-tab="four">
    <i class="archive icon"></i>
    Guias
  </a>
  <div class="ui item right dropdown">
    <i class="talk icon"></i>
    Redactar
    <div class="menu">
      @if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '3') || (Auth::user()->username == $dato->autor))
        <a class="item" href="{{route('noticias.create')}}"> <i class="newspaper icon"></i> Noticia</a>
      @endif
        <a class="item" href="{{route('preguntas.create')}}"> <i class="help icon"></i> Pregunta</a>
      @if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '2') || (Auth::user()->tipo == '3') || (Auth::user()->username == $dato->autor))
        <a class="item" href="{{route('ejercicios.create')}}"> <i class="rocket icon"></i> Reto</a>
      @endif
      @if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '2') || (Auth::user()->tipo == '3') || (Auth::user()->username == $dato->autor))
        <a class="item" href="{{route('guias.create')}}"> <i class="archive icon"></i> Guia</a>
      @endif
    </div>
  </div>
</div>
<br>
<div class="ui bottom tab active" data-tab="first">
  @include('secciones.noticias')  
</div>
<div class="ui bottom tab" data-tab="second">
  @include('vistas.preguntas')  
</div>
<div class="ui bottom tab" data-tab="third">
  @include('vistas.ejercicios')  
</div>
<div class="ui bottom tab" data-tab="four">
  @include('vistas.guias')  
</div>