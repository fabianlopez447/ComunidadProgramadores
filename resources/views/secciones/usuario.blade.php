<div class="ui labeled icon attached tabular menu pestana">
  <a class="item active" data-tab="second">
    <i class="help icon"></i>
    Mis Preguntas
  </a>
  <a class="item" data-tab="third">
    <i class="rocket icon"></i>
    Mis Retos
  </a>
  <a class="item" data-tab="four">
    <i class="archive icon"></i>
    Mis Guias
  </a>
  <div class="ui item right dropdown">
    <i class="talk icon"></i>
    Redactar
    <div class="menu">
      @if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '3') || (Auth::user()->username == $dato->autor))
        <a class="item" href="{{route('noticias.create')}}"> <i class="newspaper icon"></i> Noticia</a>
      @endif
        <a class="item" href="{{route('preguntas.create')}}"> <i class="help icon"></i> Pregunta</a>
      @if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '2') || (Auth::user()->tipo == '3') || (Auth::user()->username == $dato->autor))
        <a class="item" href="{{route('ejercicios.create')}}"> <i class="rocket icon"></i> Reto</a>
      @endif
      @if((Auth::user()->tipo == '0') || (Auth::user()->tipo == '2') || (Auth::user()->tipo == '3') || (Auth::user()->username == $dato->autor))
        <a class="item" href="{{route('guias.create')}}"> <i class="archive icon"></i> Guia</a>
      @endif
    </div>
  </div>
</div>
<br>
<div class="ui bottom tab active" data-tab="second">
  @include('vistasPerfil.preguntas')  
</div>
<div class="ui bottom tab" data-tab="third">
  @include('vistasPerfil.ejercicios')  
</div>
<div class="ui bottom tab" data-tab="four">
  @include('vistasPerfil.guias')  
</div>