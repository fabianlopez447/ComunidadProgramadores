
<h2 class="ui header paginacion">Ultimas Guias</h2>

<div class="ui grid" style="padding-top: 1em;">
	@foreach($guias as $guia)
	<div class="eight wide column">
		<div class="ui card">
			<div class="content">
				<div class="header"><h3 class="ui header paginacion">{{$guia->titulo}}</h2></div>
				<div class="meta">{{$guia->created_at}}</div>
			</div>
			<div class="extra content">
				<a href="#">
					<i class="check icon"></i>
					Ver esta guia
				</a>
			</div>
		</div>	
	</div>
	@endforeach
</div>