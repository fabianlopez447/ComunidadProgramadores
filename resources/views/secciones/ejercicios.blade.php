<?php
 $ejercicio= \App\Ejercicio::orderBy('created_at', 'DESC')->paginate(6);
?>
<div class="ui card">
	<div class="content">
		<div class="header"> <center> <i class="circular trophy icon"></i>Ultimos ejercicios</center></div>
	</div>
	@foreach($ejercicio as $ejercicios)
	<div class="content">
		<h4 class="ui sub header">{{$ejercicios->titulo}}</h4>
		<a href="{{route('ejercicios.ver',$ejercicios->id)}}">Leer más</a>
	</div>
	@endforeach
	<div class="extra content">
		<center><a href="{{route('ejercicios.index')}}" class="ui">Más Ejercicios</a></center>
	</div>
</div>