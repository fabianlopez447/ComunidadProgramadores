<?php
 $pregunta= \App\Pregunta::orderBy('created_at', 'DESC')->paginate(6);
?>
<div class="ui card p-bottom-md shadow-bottom">
	<div class="content">
		<div class="header"> <center><strong>Ultimas preguntas</strong></center></div>
	</div>
	@foreach($pregunta as $preguntas)
	<div class="content">
		<h4 class="ui sub header">
			<strong>{{ $preguntas->titulo }}</strong>
		</h4>
		<a href="{{route('preguntas.ver',$preguntas->id)}}">Leer más</a>
	</div>
	@endforeach
	<div class="extra content">
		<center><a href="{{route('preguntas.index')}}"><button class="ui button teal">Más Preguntas</button></a></center>
	</div>
</div>
