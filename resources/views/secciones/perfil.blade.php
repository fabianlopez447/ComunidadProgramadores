
<div class="ui card border-card p-bottom-md shadow-bottom">

	<div class="image" style="position: relative;" id="imagen">
		<a href="{{route('perfil.foto.edit', Auth::user()->id)}}" title="Editar foto de perfil" style="width:100%;" class="ui  image">
			<img id="img" style="width:100%;" src="{!!asset('img/perfil/'.Auth::user()->foto)!!}">
		</a>
		
	</div>
	<div class="content" style="position: relative;">
		<a class="header" title="{{Auth::user()->nombre}} {{Auth::user()->apellido}}">
			{{ Auth::user()->username }}
		</a>
		
		<div class="meta">
			<span class="date">Activo desde: {{date('Y', strtotime(Auth::user()->created_at))}}</span>
		</div>
		<div class="description">Puntos {{Auth::user()->puntuacion}}</div>
	</div>
	<div class="extra content">
		<i class="user admin "></i> 
		@if(Auth::user()->tipo==0)
			Administrador 
		@endif
		@if(Auth::user()->tipo==1)
			Miembro 
		@endif
		@if(Auth::user()->tipo==2)
			Editor 
		@endif
		@if(Auth::user()->tipo==3)
			Moderador 
		@endif
		</p>
	</div>
</div> 	
