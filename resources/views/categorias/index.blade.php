@extends('layouts.template')
@section('content')
	<div class="container ui">
		<div class="ui grid">
			<div class="three column row">	
				<!-- Perfil de Usuario -->
				<div class="column three wide">
					@include('secciones.perfil')
				<!-- Ultimas Preguntas-->
				    @include('secciones.preguntas')
				</div>
				<div class="nine wide column ">
					<div style="border-radius:0px;"  class="">				
					<center>	
						<h2 class="ui center aligned icon">
							<i class="circular send icon"></i>Categorías
						</h2>		
					</center>
					@foreach($categoria as $categorias)
					<div style="border-radius:0px;" class="ui info message">
						<div class="header">
							{!!$categorias->id!!}
							{!!$categorias->nombre!!}
						</div>
						
					</div>
					@endforeach
					</div>
					<br>
					<a href="{{ route('categorias.create')}}"><button class="ui button">Crear Categoría</button></a>
				</div>
				<div  class="four wide column">
					<!-- Ultimos ejercicios -->
					@include('secciones.ejercicios')	
				</div>
			</div>
		</div>
	</div>
@endsection