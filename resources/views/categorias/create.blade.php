@extends('layouts.template')
@section('content')
<div class="container ui">
		<div class="ui grid">
			<div class="three column row">	
				<!-- Perfil de Usuario -->
				<div class="column three wide">
					@include('secciones.perfil')
				<!-- Ultimas Preguntas-->
				    @include('secciones.preguntas')
				</div>
				<div class="thirteen wide column ">
					<div style="border-radius:0px;"  class="ui raised segment ">					
						<h2 class="ui center aligned icon">
							Nueva Categoría<hr>
						</h2>	
						{!!Form::open(['route'=>'categorias.store','method'=>'POST', 'class'=>'ui form ingresar', 'files' => true, 'enctype'=>'multipart/form-data'])!!}
						<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
							<div class="fields">
								<div class="sixteen wide field">
									<label>Nombre</label>
									{!!Form::text('nombre', null,['placeholder'=>'Nombre de la categoría'])!!}
								</div>
							</div>
							<button type="submit" class="ui primary submit button">Aceptar</button>
					   {!!Form::close()!!}
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
@section('js')

@endsection