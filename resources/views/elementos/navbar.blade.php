<!-- Navbar -->
	<navbar>
			<div style="border-radius:0px;margin-bottom: 2em;" class="ui inverted segment ">
			  <div class="ui inverted secondary pointing menu center">
			  	<div class="ui container ">
			  	@if(Auth::user())
				    <a href="{{route('index')}}" class="active item" href="index.html">Inicio </a>
					<a class="item " href="{{ route('perfil.index') }}">Perfil</a>		
					<a class="item right" href="{{ route('logout') }}">Cerrar sesión</a>
				@else
					<a class="item" href="{{ url('/login') }}">Iniciar sesión</a>
	                <a class="item" href="{{ url('/register') }}">Registrarse</a>
				@endif	
				</div> 
			  </div> 
			 </div>			 	
	</navbar>
<!-- Fin Navbar -->